#!/bin/bash
pwd
kubectx home
kubeseal  --controller-name sealed-secrets --controller-namespace default --format yaml < ./secrets/cluster-secrets.yaml > cluster/cluster-secrets.yaml
git add ./cluster/cluster-secrets.yaml
